//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <math.h>

class MidiMessage
{
public:
    MidiMessage()//Constructor
    {
        number = 60;
        vel = 90;
        chan = 1;
    }
    ~MidiMessage() //Destructor
    {
    }
    void setNoteNumber (int value) //Mutator
    {
        if(value>=0 && value<= 127)
        {
            number = value;
        }
        
    }
    int getNoteNumber() const //Accessor
    {
        return number;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    void setVelocityNumber (int value) //Mutator
    {
        if(value>=0 && value<= 127)
        {
            vel = value;
        }
        
    }
    int getVelocityNumber() const //Accessor
    {
        return vel;
    }
    void setChannelNumber (int value) //Mutator
    {
        if(value>=0 && value<= 15)
        {
            chan = value;
        }
    }
    int getChannelNumber() const //Accessor
    {
        return chan;
    }
private:
    int number;
    int vel;
    int chan;
};

int main (int argc, const char* argv[])
{
    MidiMessage number;
    int num;
    float freq;
    int velo;
    int channel;
    
    freq = number.getMidiNoteInHertz();
    
    num = number.getNoteNumber();
    
    velo = number.getVelocityNumber();
    
    channel = number.getChannelNumber();
    
    std::cout << "\n" << num << "\nfreq = "<<freq;
    std::cout << "velocity=" << velo << " channel:" << channel;
    
    std::cin >> num;
    
    number.setNoteNumber(num);
    
    freq = number.getMidiNoteInHertz();
    
    num = number.getNoteNumber();
    
    
    std::cout << "\n" << num << "\nfreq = "<<freq;

    
    return 0;
}
